# SafetyCar Project
This is sample repository for testing the SafetyCar Web Application

- Clone repository https://gitlab.com/TelerikAcademy/telerik-academy-alpha-qa-make-up-project/tree/master/Final%20Project
- With HeidiSQL load the database into db folder
- Load the project in IntelliJ IDEA
- run the SafetyCarApplication class
- Acces the application locally (localhost:8080)

API tests with Postman
We test the following site: https://gorest.co.in/
Here are my API service tests, made in Postman.
With a registration in Postman or with your gmail account, you can see and explore the tests collection with token=YxpdTLOB9HngcUFPGj-rkC3C3KnPkixp8.

How to run Postman with Newman
Newman is a command line Collection Runner for Postman. It allows you to run and test a Postman Collection directly from the command line.
Newman is built on Node.js. To run Newman, make sure you have Node.js installed.
 - npm install -g newman
 - newman run https://www.getpostman.com/collections/c0c30f599cc728f40cf8   


UI automation testing with Selenium: 

For our GUI tests I have:
 1. Java programming language: Java JDK 8
 2. InteliJ IDEA ultimate editor
 3. Gecko driver exe-s
 4. Firefox browser
 5. We use Selenium Webdriver - an object-oriented automation API that nativly drives a browser as an user would.

I use Maven for my framework. In the POM file I add all dependencies for Java, Selenium webdriver, TestNG.
For my Selenium Automation testing project I have two layers: framework lawer and test lawer.
The framework is in src/main/java. There are all of the interactions with the web browser and all of the calls in the application.
The tests are in scr/test/java. There I focus to the tests itself.
I used a pattern - page object model design pattern.This mean every page of the application is repressenting from a java class in package "pages" in out framwork in src/main.
All of my test cases are in the src/test folder.
In class "BaseTests" I have different tests and all of these tets create an end to end scenario. I set up a web driver with a method with annotation @BeforeClass and quit it with a method with annotation @AfterClass.


