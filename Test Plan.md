# TEST PLAN

  
  | Date  | Version | Description           | Author       | Reviewer | Approver |
  |-------|---------|-----------------------|--------------|----------|----------|
  | 12.02 | 0.2     | Test plan was created | M. Yordanov  |          |          |

## Contents
    1. INTRODUCTION	
    1.1 Objectives	
    2.0 SCOPE	
    2.1 Features To Be Tested	
    2.2 Features Not To Be Tested	
    3.0 APPROACH	
    4.0 TESTING PROCESS	
    4.1 Test Deliverables	
    4.2 Responsibilities	
    4.3 Resources	
    4.4 Estimation and Schedule	
    5.0 ENVIRONMENT REQUIREMENTS

### 1.Introduction

#### 1.2 Objective
The customer wants a perfectly working API for car insurance, which passed the full cycle of manual and automation testing.This document describes approaches and methodologies that will apply to the unit, integration and system testing of the Safety Car API.
It includes the scope, features to be tested and what not to be tested, testing process, approach, testing process identify the methods and criteria used in performing test activities, test deliverables, responsibilities and resources, and estimation of the schedule.

#### 1.1.2 Primary Objectives
A primary objective of testing is to: assure that the system meets the full requirements, including quality requirements (functional and nonfunctional requirements) and fit metrics for each quality requirement and satisfies the use case scenarios and maintains the quality of the product.

#### 1.1.3 Secondary Objectives
The secondary objectives of testing will be to: identify and expose all issues and associated risks, communicate all known issues to the project team, and ensure that all issues are addressed in an appropriate matter before release.

### Scope
The document mainly targets the GUI testing and validating data in report output as per Requirements Specifications provided by Client.

#### 2.1 Features to Be Tested

* Login form
* Registration form
* Create Insurance request
* Pending, Approved,Declined ,Canceled ,Expired requests
* Logout 


#### 3 Approach
The approach, that used, is Analytical therefore, in accordance with the requirements-based strategy, where an analysis of the requirements specification forms the basis for planning, Test Plan estimating and designing tests. Test cases will be created during exploratory testing. All test types are determined in Test Process. The project is using an agile approach, with weekly iterations.

### 4 Testing Process

#### 4.1 Entry Criteria
- All the necessary documentation, design, and requirements information should be
available that will allow the tester to operate the system and judge the correct behavior.
- All the standard software tools including the testing tools must have been
successfully installed and functioning properly.
- The test environment such as lab, hardware, software, and system administration support should be ready.
- QA resources have completely understood the requirements.

#### 4.2 Exit criteria
- 100% of Critical test cases pass
- 80 % of High test cases pass

#### 4.3 Testing Types
* Black box Testing: It is some behavioral testing or Partitioning testing. This kind of testing focuses on the time functional requirements of the software. It enables one to derive sets of input conditions that will fully exercise all functional requirements for the web application.

* Exploratory testing is all about discovery, investigation, and learning. It emphasizes the personal freedom and responsibility of the individual tester. It is defined as a type of testing where Test cases are not created in advance but tester check system on the fly.

* GUI Testing: GUI testing will include testing the UI part of the report. It covers the user's Report format, looks, and feel, error messages, spelling mistakes, GUI guideline violations.

* Integration Testing: used to test individual software components or units of code to verify the interaction between various software components and detect interface defects. Components are tested as a single group or organized in an iterative manner. After the integration testing has been performed on the components, they are readily available for system testing.

* Functional Testing: Functional testing is carried out in order to find out the unexpected behavior of the report. The characteristic of functional testing is to provide correctness, reliability, testability, and accuracy of the report output/data.

* System Testing: System testing of software is testing conducted on a complete, integrated system to evaluate the system's compliance with its specified requirements.

* Usability testing -  Testing to determine the extent to which the software product is understood, easy to learn, easy to operate and attractive to the users under specified conditions.

#### 4.4 Bug Severity and Priority fields

| Severity ID | Severity | Severity Description                                                                         |
|-------------|----------|----------------------------------------------------------------------------------------------|
| 1           | Critical | Showstopper, major failure that affects the entire system or multiple modules of the system. |
| 2           | High     | This is big, a major piece of functionality of the application is broken                     |
| 3           | Medium   | The feature is not working correctly, but it will not impact users greatly                   |
| 4           | Minor    | Could live with the issue, should be fixed when possible                                     |


| Priority  ID | Priority  | Priority Description |
|--------------|-----------|----------------------|
| 1            | Priority1 | Must Fix             |
| 2            | Priority2 | Should Fix           |
| 3            | Priority3 | Fix When Have Time   |
| 4            | Priority4 | Low Priority         |

#### 4.5  Test Deliverables
1. Test Plan
2. High Level Test Cases
3. API Tests
4. UI tests
5. Test Report
6. Bug report

#### 4.6 Responsibilities

QA role in the test process
- Preparing Test Cases:
	- Reviewing test cases.
	- Creating Test Data.
- Executing Test Cases: manual and automation 

### 5 Resources

#### 5.1 Testing Tool

| Process              | Testing Tool                                   |
|----------------------|------------------------------------------------|
| Create test case     | TestRail                                       |
| Test case tracking   | TestRail                                       |
| Test case execution  | Manual, Selenium, Postman, Test NG, Jbehave    |
| Test case management | Trello                                         |
| Defect management    | Gitlab                                         |
| Test reporting       | TestRail                                       |


#### 5.2 Configuration management

* Documents: Trello, TestRail
* Code: Gitlab

#### 5.3 Estimation and Schedule

| Task name                                          | Start | Finish | Effort | Comments |
|----------------------------------------------------|-------|--------|--------|----------|
| Test Planning                                      |23.12  |24.12   |        |          |
| Review Requirements documents                      |29.12  |01.01   |        |          |
| Create high level test cases                       |27.12  |27.01   |        |          |
| Start and train new test resources                 |01.01  |10.02   |        |          |
| Exploratory testing - manual check for happy path  |10.01  |22.01   |        |          |
| Automation tests - happy paths / Selenium, TestNG/ |23.01  |1.02    |        |          |
| Jbehave for BDD scenarios                          |30.01  |        |        |          |
| API testing - Postman                              |16.01  |29.01   |        |          |
| Report - Automation tests                          |03.02  |03.02   |        |          |
| Create a coverage report                           |02.02  |02.02   |        |          |
| Create bat/script file for automation tests        |01.02  |01.02   |        |          |

### 6 ENVIRONMENT REQUIREMENTS

Test Team  - 1 QA tester

* Support level 1 (browsers); 
    * Firefox(69.0.3 (64-bit))
    * Google Chrome 80.0.3987.87(64 бита) 
* Support level 2 (devices)
    * Dell Inspiron N5110, Intel i3-2110M, 8GB Memory, 500 HDD, Intel HD Graphics 3000,  1366x768, 15.6"

#### 6.1 TERMS/ACRONYMS 
* The below terms are used as examples, please add/remove any terms relevant to the document. 

| TERM/ACRONYM | DEFINITION                    |
|--------------|-------------------------------|
| API          | Application Program Interface |
| GUI          | Graphical user interface      |
| UAT          | User acceptance testing       |
| QA           | Quality Assurance             |
