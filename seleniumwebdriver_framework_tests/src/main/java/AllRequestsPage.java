import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class AllRequestsPage extends BasePage {

    By elements = By.xpath("//div[@class='col-md-6']");

    public void test() {
        List<WebElement> list =  getDriver().findElements(elements);
        System.out.println("the elements qtty is :" + list.size());
        WebElement acceptButton = list.get(0).findElement(By.xpath("//button[@type='submit'] [@value='approved']"));
        acceptButton.click();

    }
}
