import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;


class BasePage extends DriverFactory {

    private By profileDropdownButton = By.xpath("//a[contains(text(),'admin45')]");
    private By uploadTableButton = By.xpath("//a[contains(text(),'Upload')]");
    private By logOutButton = By.xpath("//a[contains(text(),'Logout')]");
    private By logoButtonSafetyCar = By.xpath("//a[@class='navbar-brand']//img");


    public void clickLogoButtonSafetyCar() {
        clickElement(logoButtonSafetyCar);
    }

    void clickElement(By by) {

        waitForElementToBeVisible(by);
        getDriver().findElement(by).click();
    }
    void waitForElementToBeVisible(By by) {

        WebElement element = (new WebDriverWait(getDriver(), 15))
                .until(ExpectedConditions.presenceOfElementLocated(by));


    }


    void fillField(By by, String text) {
        waitForElementToBeVisible(by);
        getDriver().findElement(by).sendKeys(text);
    }

    public void quitDriver() {
        getDriver().quit();
    }

    public void logOut() {
        clickElement(profileDropdownButton);
        clickElement(logOutButton);
    }

    public void goToUploadPage() {
        clickElement(profileDropdownButton);
        clickElement(uploadTableButton);

    }

    public void verifyPageTitle(String title) {

        junit.framework.Assert.assertEquals(getDriver().getTitle(), title);
    }

}
