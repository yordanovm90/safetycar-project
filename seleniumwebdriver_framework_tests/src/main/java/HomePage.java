import org.openqa.selenium.By;

public class HomePage extends BasePage {


    private static final String baseUrl = "http://localhost:8080/";
    private By loginButton = By.xpath("//a[contains(text(),'Login')]");
    private By insuranceCalculatorNavButton = By.xpath("//li[@class='scroll']//a[contains(text(),'Insurance Calculator')]");
    private By insuranceCalculatorHomeButton = By.xpath("//a[@class='btn btn-primary btn-lg']");
    private By allRequestNavButton = By.xpath("//a[contains(text(),'All Requests')]");

    public HomePage goToHomePage() {
        getDriver().get(baseUrl);
        return this;
    }


    public void clickInsuranceCalculatorHomeButton() throws InterruptedException {
        clickElement(insuranceCalculatorNavButton);
        Thread.sleep(1000);
        clickElement(insuranceCalculatorHomeButton);

    }

    public void clickInsuranceCalculatorButton(){

        clickElement(insuranceCalculatorNavButton);
    }

    public HomePage clickLoginButton() {
        clickElement(loginButton);
        return this;
    }

    public HomePage clickAllRequestNavButton() {
        clickElement(allRequestNavButton);
        return this;
    }
}

