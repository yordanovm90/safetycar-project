import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public class InsuranceCalculatorPage extends BasePage {

    private By calculateButton = By.xpath("//button[@class='login100-form-btn calculate']");
    private By driverAgeField = By.id("driver-age-id");
    private By cubicCapacityField = By.id("cubic-capacity-field");
    private By carField = By.id("carBrand");
    private By carModelField = By.xpath("//select[@id='carModel']");
    private By carAccidentBox = By.id("offer.hadAccident1");
    private By firstRegistrationField = By.name("offer.firstRegDate");
    private By generateOfferButton = By.xpath("//button[@class='login100-form-btn scroll']");


    public InsuranceCalculatorPage clickGenerateOfferButton(){
        clickElement(generateOfferButton);
        return this;
    }
    public InsuranceCalculatorPage clickCalculateButton(){
        clickElement(calculateButton);
        return this;
    }

    public InsuranceCalculatorPage fillDriverAgeField(String age){
        fillField(driverAgeField, age);
        return this;
    }

    public InsuranceCalculatorPage fillCubicCapacityField(String capacity){
        fillField(cubicCapacityField, capacity);
        return this;
    }

    public InsuranceCalculatorPage selectCar(String carMake) {
        WebElement car = getDriver().findElement(carField);
        Select carSelect = new Select(car);
        carSelect.selectByVisibleText(carMake);
        return this;
    }

    public InsuranceCalculatorPage selectModelCar() {
        WebElement car = getDriver().findElement(carModelField);
        Select carSelect = new Select(car);
        carSelect.selectByIndex(10);
        return this;
    }

    public InsuranceCalculatorPage carAccidentClick() {
            clickElement(carAccidentBox);
        return this;
    }

    public InsuranceCalculatorPage firstRegistrationDateField(String day, String month, String year) {
        WebElement birthDate= getDriver().findElement(firstRegistrationField);
        birthDate.sendKeys(year + "-" + month + "-" + day);
        return this;
    }

    @Override
    public void verifyPageTitle(String title) {
        ArrayList<String> tabs2 = new ArrayList<String> (getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs2.get(1));
        junit.framework.Assert.assertEquals(getDriver().getTitle(), title);
//        getDriver().close();
//        getDriver().switchTo().window(tabs2.get(0));
    }

}
