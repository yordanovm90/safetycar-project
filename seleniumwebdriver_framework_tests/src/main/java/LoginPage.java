import org.openqa.selenium.By;

public class LoginPage extends BasePage {

    private By usernameField = By.id("username-field");
    private By passwordField = By.id("password-field");
    private By loginButton = By.xpath("//button[@class='login100-form-btn']");


    public LoginPage fillUsernameFiled(String username) {
        fillField(usernameField, username);
        return this;
    }

    public LoginPage fillPasswordFiled(String password) {
        fillField(passwordField, password);
        return this;
    }

    public LoginPage clickLoginButton() {
        clickElement(loginButton);
        return this;
    }


}
