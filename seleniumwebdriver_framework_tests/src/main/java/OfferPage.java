import org.jsoup.Connection;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.File;

public class OfferPage extends BasePage {


    private By effectiveDate = By.id("effectiveDate");
    private By docImage = By.id("docimage");
    private By submitButton = By.xpath("//button");


    public OfferPage effectiveDateField(String day, String month, String year) {
        WebElement birthDate = getDriver().findElement(effectiveDate);
        birthDate.sendKeys(year + "-" + month + "-" + day);
        return this;
    }

    public OfferPage uploadImg() throws InterruptedException {

        File picture = new File("src/main/resources/pic.jpg");
        fillField(docImage,picture.getAbsolutePath());

        Thread.sleep(1000);
        return this;
    }

    public OfferPage clickSubmitButton() {

        clickElement(submitButton);
        return this;
    }

}
