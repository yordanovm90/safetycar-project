import org.openqa.selenium.By;

import java.io.File;

public class UploadPage extends BasePage {

    private By attachFile = By.id("exceltable");
    private By uploadButton = By.xpath("//button[@class='btn btn-primary btn-lg']");


    public void attachFile() throws InterruptedException {

        File exceldatabase = new File("src/main/resources/exceldatabase.xlsx");
        exceldatabase.getAbsolutePath();
        fillField(attachFile,exceldatabase.getAbsolutePath());
        clickElement(uploadButton);

    }

}
