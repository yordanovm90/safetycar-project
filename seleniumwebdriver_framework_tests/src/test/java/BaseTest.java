import org.testng.annotations.*;


public class BaseTest {

    public HomePage homePage;
    public LoginPage loginPage;
    public UploadPage uploadPage;
    public InsuranceCalculatorPage insuranceCalculatorPage;
    public OfferPage offerPage;
    public AllRequestsPage allRequestsPage;

    @Test(priority = 1)
    public void goToHomePageTest() {
        homePage.goToHomePage();
        homePage.verifyPageTitle("Safety Car");
    }

    @Test(priority = 2)
    public void loginTest() {

        homePage.clickLoginButton();

        loginPage.
                fillUsernameFiled("admin45")
                .fillPasswordFiled("p@ssword")
                .clickLoginButton();

        homePage.verifyPageTitle("Safety Car");

    }

    @Test(priority = 3)
    public void uploadCalculatorTableTest() throws InterruptedException {
        homePage.goToUploadPage();
        uploadPage.verifyPageTitle("My Profile");
        uploadPage.attachFile();
        uploadPage.clickLogoButtonSafetyCar();

    }

    @Test(priority = 4)
    public void createOfferTest() throws InterruptedException {
        Thread.sleep(1000);
        homePage.clickInsuranceCalculatorHomeButton();
        insuranceCalculatorPage.selectCar("BMW");
        insuranceCalculatorPage.firstRegistrationDateField("22", "01", "1999");
        insuranceCalculatorPage.selectModelCar();
        insuranceCalculatorPage.fillCubicCapacityField("2000");
        insuranceCalculatorPage.fillDriverAgeField("28");
        insuranceCalculatorPage.carAccidentClick();

        insuranceCalculatorPage.clickCalculateButton();
        Thread.sleep(2000);

        insuranceCalculatorPage.clickGenerateOfferButton();

        insuranceCalculatorPage.verifyPageTitle("Offer");
    }

    @Test(priority = 5)
    public void offerTest() throws InterruptedException {
        offerPage.effectiveDateField("20", "05", "2020");
        offerPage.uploadImg();

        offerPage.clickSubmitButton();
        offerPage.verifyPageTitle("My requests");
    }

    @Test(priority = 6)
    public void approveOfferTest() throws InterruptedException {
//        Thread.sleep(2000);
        homePage.clickAllRequestNavButton();
        allRequestsPage.test();


    }


    @Test(priority = 7 )
    public void logOutTest() {
        homePage.logOut();
    }

    @BeforeClass
    public void classSetUp() {
        homePage = new HomePage();
        loginPage = new LoginPage();
        uploadPage = new UploadPage();
        insuranceCalculatorPage = new InsuranceCalculatorPage();
        offerPage = new OfferPage();
        allRequestsPage = new AllRequestsPage();

    }

    @AfterClass
    public void tearDown() {
        homePage.quitDriver();
    }
}
